<?php

namespace Slts\Flysystem\DI;


use League\Flysystem\Config;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemInterface;
use League\Flysystem\MountManager;
use Nette\DI\CompilerExtension;
use Nette\DI\ContainerBuilder;
use Nette\DI\Statement;
use Slts\Flysystem\Exception\ServiceConfigurationException;

class FlysystemExtension extends CompilerExtension
{

    private $defaults = [
        'adapters' => [],
        'filesystems' => [],
        'mountManagers' => [],
    ];

    private $adapterConfTemplate = [
        'class' => '',
        'autowired' => false,
    ];

    private $filesystemConfTemplate = [
        'adapter' => '',
        'config' => [],
        'autowired' => false,
        'plugins' => [],
    ];

    private $mountManagerConfTemplate = [
        'filesystems' => [],
        'autowired' => false,
    ];

    public function loadConfiguration()
    {
        $builder = $this->getContainerBuilder();
        $config = $this->getConfig($this->defaults);

        if (empty($config['adapters'])) {
            return;
        }

        $allAdapters = $this->loadAdapters($builder, $config);
        $allFilesystems = $this->loadFilesystems($builder, $config, $allAdapters);
        $this->loadMountManagers($builder, $config, $allFilesystems);
    }

    private function loadAdapters(ContainerBuilder $builder, array $config)
    {
        $allAdapters = [];
        foreach ($config['adapters'] as $adapterName => $adapter) {
            $adapterConf = $this->validateConfig($this->adapterConfTemplate, $adapter);

            $class = $adapterConf['class'];
            $adapterDef = $builder->addDefinition($this->prefix("adapter.{$adapterName}"));
            
            if ($class instanceof Statement) {
                $adapterDef->setFactory($class->getEntity())
                    ->setArguments($class->arguments);
            } elseif (is_string($class)) {
                $adapterDef->setFactory($class);
            } else {
                throw ServiceConfigurationException::invalidAdapterClassDefinition($adapterName);
            }

            $allAdapters[$adapterName] = $adapterDef->setAutowired($adapterConf['autowired']);
        }

        return $allAdapters;
    }

    private function loadFilesystems(ContainerBuilder $builder, array $config, array $allAdapters)
    {
        $allFilesystems = [];
        $allPlugins = [];
        foreach ($config['filesystems'] as $fsName => $filesystem) {
            $filesystemConf = $this->validateConfig($this->filesystemConfTemplate, $filesystem);

            if (!$filesystemConf['adapter']) {
                throw ServiceConfigurationException::missingAdapterDefinition($fsName);
            }
            if (!isset($allAdapters[$filesystemConf['adapter']])) {
                throw ServiceConfigurationException::unknownAdapter($filesystemConf['adapter']);
            }

            $filesystemDef = $builder->addDefinition($this->prefix("filesystem.{$fsName}"));
            $filesystemDef->setFactory(
                Filesystem::class,
                [
                    $allAdapters[$filesystemConf['adapter']],
                    new Statement(Config::class, [$filesystemConf['config']])
                ]
            );
            if ($filesystemConf['autowired']) {
                $filesystemDef->setAutowired(FilesystemInterface::class);
            }

            foreach ($filesystemConf['plugins'] as $plugin) {
                if (!isset($allPlugins[$plugin])) {
                    $pluginName = str_replace('\\', '_', $plugin);
                    $allPlugins[$plugin] = $builder->addDefinition($this->prefix("plugin.{$pluginName}"))
                        ->setFactory($plugin)
                        ->setAutowired(false);
                }

                $filesystemDef->addSetup('addPlugin', [$allPlugins[$plugin]]);
            }

            $allFilesystems[$fsName] = $filesystemDef;
        }

        return $allFilesystems;
    }

    private function loadMountManagers(ContainerBuilder $builder, array $config, array $allFilesystems)
    {
        foreach ($config['mountManagers'] ?? [] as $mmName => $mountManager) {
            $mountManagerConf = $this->validateConfig($this->mountManagerConfTemplate, $mountManager);

            $argFilesystems = [];
            foreach ($mountManagerConf['filesystems'] as $name => $fs) {
                if (!isset($allFilesystems[$fs])) {
                    throw ServiceConfigurationException::unknownFilesystem($fs);
                }

                $key = is_string($name) ? $name : $fs;
                $argFilesystems[$key] = $allFilesystems[$fs];
            }

            $def = $builder->addDefinition($this->prefix("mountManager.{$mmName}"))
                ->setFactory(
                    MountManager::class,
                    [
                        $argFilesystems
                    ]
                )
            ;
            if ($mountManagerConf['autowired']) {
                $def->setAutowired(MountManager::class);
            }
        }
    }

}