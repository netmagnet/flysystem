<?php

namespace Slts\Flysystem\Exception;

use InvalidArgumentException;

class ServiceConfigurationException extends InvalidArgumentException
{

    public static function unknownAdapter($name)
    {
        return new static("Unknown adapter {$name}");
    }

    public static function unknownFilesystem($name)
    {
        return new static("Unknown filesystem {$name}");
    }

    public static function invalidAdapterClassDefinition($name)
    {
        return new static("Invalid class definition for adapter {$name}");
    }

    public static function missingAdapterDefinition($name)
    {
        return new static("Missing adapter definition for filesystem {$name}");
    }

}