<?php

namespace Slts\Flysystem\Exception;

use League\Flysystem\AdapterInterface;
use LogicException;

class IncompatiblePluginException extends LogicException
{

    public static function invalidAdapter(AdapterInterface $adapter, array $expectedAdapters = [])
    {
        $expected = implode(', ', $expectedAdapters);
        $bad = get_class($adapter);
        return new static("Plugin can be used only with {$expected} adapter(s), {$bad} used");
    }

    public static function missingFilesystemMethod($method)
    {
        return new static("Filesystem has to implement {$method} to support this plugin");
    }
}