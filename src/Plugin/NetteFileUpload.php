<?php

namespace Slts\Flysystem\Plugin;

use League\Flysystem\File;
use League\Flysystem\FileExistsException;
use League\Flysystem\Plugin\AbstractPlugin;
use Nette\Http\FileUpload;

class NetteFileUpload extends AbstractPlugin
{

    /**
     * @inheritdoc
     */
    public function getMethod()
    {
        return 'moveNetteFileUpload';
    }

    /**
     * @param string     $path
     * @param FileUpload $fileUpload
     *
     * @throws InvalidArgumentException If $resource is not a file handle.
     * @throws FileExistsException
     *
     * @return bool|File
     */
    public function handle($path, FileUpload $fileUpload)
    {
        if (!$fileUpload->isOk()) {
            return false;
        }

        $stream = fopen($fileUpload->getTemporaryFile(), 'r+b');
        $result = $this->filesystem->writeStream($path, $stream);
        fclose($stream);

        return $result ? new File($this->filesystem, $path) : false;
    }
}