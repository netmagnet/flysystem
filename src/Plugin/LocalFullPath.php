<?php

namespace Slts\Flysystem\Plugin;

use Slts\Flysystem\Exception\IncompatiblePluginException;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Plugin\AbstractPlugin;

class LocalFullPath extends AbstractPlugin
{

    /**
     * @inheritdoc
     */
    public function getMethod()
    {
        return 'getFullPath';
    }

    /**
     * Plugin handle method
     *
     * @param string $path
     *
     * @return string
     * @throws IncompatiblePluginException
     */
    public function handle($path)
    {
        if (!method_exists($this->filesystem, 'getAdapter')) {
            throw IncompatiblePluginException::missingFilesystemMethod('getAdapter');
        }

        $adapter = $this->filesystem->getAdapter();
        if (!$adapter instanceof Local) {
            throw IncompatiblePluginException::invalidAdapter($adapter, [Local::class]);
        }

        /** @var Local $adapter */
        return $adapter->applyPathPrefix($path);
    }
}