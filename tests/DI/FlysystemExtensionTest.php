<?php

use Nette\DI\Compiler;
use Nette\DI\Container;
use Nette\DI\ContainerLoader;
use Slts\Flysystem\DI\FlysystemExtension;

class FlysystemExtensionTest extends PHPUnit_Framework_TestCase
{
    /** @var \Nette\DI\Compiler */
    private $compiler;
    private $extension;

    public function setUp()
    {
        $containerBuilder = new \Nette\DI\ContainerBuilder();
        $compiler = new \Nette\DI\Compiler($containerBuilder);
        $extension = new FlysystemExtension();
        $compiler->addExtension('flysystem', $extension);
        
        $this->compiler = $compiler;
        $this->extension = $extension;
    }

    private function createContainer(string $config): Container
    {
        $loader = new ContainerLoader(sys_get_temp_dir(), true);
        $class = $loader->load(function (Compiler $compiler) use ($config) {
            $compiler->addExtension('flysystem', new FlysystemExtension());
            $compiler->loadConfig($config);
        }, uniqid('', true));

        /** @var Container $container */
        $container = new $class();
        return $container;
    }

    public function testLoadConfigurationAllSetCorrectly()
    {
        $container = $this->createContainer(__DIR__ . '/configs/success.neon');

        self::assertFalse($container->hasService('flysystem.adapter.noname'));
        self::assertInstanceOf(\League\Flysystem\Adapter\Local::class, $container->getService('flysystem.adapter.localwww'));
        self::assertInstanceOf(\League\Flysystem\Adapter\NullAdapter::class, $container->getService('flysystem.adapter.default'));
        self::assertInstanceOf(\League\Flysystem\Adapter\Ftp::class, $container->getService('flysystem.adapter.ftp'));

        self::assertFalse($container->hasService('flysystem.filesystem.noname'));
        self::assertInstanceOf(\League\Flysystem\FilesystemInterface::class, $container->getService('flysystem.filesystem.default'));
        self::assertInstanceOf(\League\Flysystem\FilesystemInterface::class, $container->getService('flysystem.filesystem.local'));

        self::assertFalse($container->hasService('flysystem.mountManager.noname'));
        self::assertInstanceOf(\League\Flysystem\MountManager::class, $container->getService('flysystem.mountManager.local2default'));

        self::assertInstanceOf(\League\Flysystem\Plugin\ListFiles::class, $container->getService('flysystem.plugin.League_Flysystem_Plugin_ListFiles'));
        self::assertInstanceOf(\Slts\Flysystem\Plugin\LocalFullPath::class, $container->getService('flysystem.plugin.Slts_Flysystem_Plugin_LocalFullPath'));
        self::assertInstanceOf(\Slts\Flysystem\Plugin\NetteFileUpload::class, $container->getService('flysystem.plugin.Slts_Flysystem_Plugin_NetteFileUpload'));

        self::assertSame($container->getService('flysystem.adapter.default'), $container->getByType(\League\Flysystem\AdapterInterface::class));
        self::assertSame($container->getService('flysystem.filesystem.local'), $container->getByType(\League\Flysystem\FilesystemInterface::class));
        self::assertSame($container->getService('flysystem.mountManager.local2default'), $container->getByType(\League\Flysystem\MountManager::class));
    }

    public function testLoadConfigurationNoAdaptersSet()
    {
        $container = $this->createContainer(__DIR__ . '/configs/no-adapters.neon');

        self::assertFalse($container->hasService('flysystem.adapter.localwww'));
        self::assertFalse($container->hasService('flysystem.adapter.default'));
        self::assertFalse($container->hasService('flysystem.adapter.ftp'));
        self::assertFalse($container->hasService('flysystem.filesystem.default'));
        self::assertFalse($container->hasService('flysystem.filesystem.local'));
        self::assertFalse($container->hasService('flysystem.mountManager.local2ftp'));
        self::assertFalse($container->hasService('flysystem.plugin.League_Flysystem_Plugin_ListFiles'));
        self::assertFalse($container->hasService('flysystem.plugin.Slts_Flysystem_Plugin_LocalFullPath'));
        self::assertFalse($container->hasService('flysystem.plugin.Slts_Flysystem_Plugin_NetteFileUpload'));
    }

    public function testLoadConfigurationNoAdapterForFilesystem()
    {
        $this->expectException(\Slts\Flysystem\Exception\ServiceConfigurationException::class);
    }

    public function testLoadConfigurationBadAdapterForFilesystem()
    {
        $this->expectException(\Slts\Flysystem\Exception\ServiceConfigurationException::class);
    }

    public function testLoadConfigurationBadFilesystemsForMountmanager()
    {
        $this->expectException(\Slts\Flysystem\Exception\ServiceConfigurationException::class);
    }
}
