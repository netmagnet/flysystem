<?php

use Slts\Flysystem\Plugin\LocalFullPath;


class LocalFullPathTest extends PHPUnit_Framework_TestCase
{

    /** @var LocalFullPath */
    protected $plugin;


    public function setUp()
    {
        $this->plugin = new LocalFullPath();
    }

    public function testPluginSuccess()
    {
        $filesystem = Mockery::mock(\League\Flysystem\Filesystem::class);
        $this->plugin->setFilesystem($filesystem);
        $filesystem->shouldReceive('getAdapter')->andReturnUsing(function (){
            return Mockery::mock(\League\Flysystem\Adapter\Local::class)
                ->shouldReceive('applyPathPrefix')
                ->with('path')
                ->andReturn('fullpath')
                ->getMock();

        })->getMock()->makePartial();

        $this->assertSame('fullpath', $this->plugin->handle('path'));
    }

    public function testPluginIncompatiblePluginExceptionMissingMethod()
    {
        $filesystem = Mockery::mock(\League\Flysystem\FilesystemInterface::class);
        $this->plugin->setFilesystem($filesystem);
        $this->expectException(\Slts\Flysystem\Exception\IncompatiblePluginException::class);

        $this->assertSame('fullpath', $this->plugin->handle('path'));
    }

    public function testPluginIncompatiblePluginExceptionInvalidAdapter()
    {
        $this->expectException(\Slts\Flysystem\Exception\IncompatiblePluginException::class);

        $filesystem = Mockery::mock(\League\Flysystem\Filesystem::class);
        $this->plugin->setFilesystem($filesystem);
        $filesystem->shouldReceive('getAdapter')->andReturnUsing(function (){
            return Mockery::mock(\League\Flysystem\Adapter\NullAdapter::class);

        })->getMock()->makePartial();

        $this->assertSame('fullpath', $this->plugin->handle('path'));
    }

}
