<?php

use Slts\Flysystem\Plugin\NetteFileUpload;
use Hamcrest\Matchers as m;

class NetteFileUploadTest extends PHPUnit_Framework_TestCase
{

    /** @var  \League\Flysystem\FilesystemInterface|\Mockery\Mock */
    protected $filesystem;

    /** @var NetteFileUpload */
    protected $plugin;

    /** @var \Nette\Http\FileUpload|\Mockery\Mock */
    protected $fileUpload;

    protected $tmpFilePath;

    protected $tmpHandle;

    public function setUp()
    {
        $this->filesystem = Mockery::mock(\League\Flysystem\FilesystemInterface::class);
        $this->plugin = new NetteFileUpload();
        $this->plugin->setFilesystem($this->filesystem);

        $this->tmpHandle = tmpfile();
        $this->tmpFilePath = stream_get_meta_data($this->tmpHandle)['uri'];
        $this->fileUpload = Mockery::mock(\Nette\Http\FileUpload::class, [
            ['name' => 'test', 'tmp_name' => $this->tmpFilePath]
        ]);
    }

    public function testPluginSuccess()
    {
        $this->assertSame('moveNetteFileUpload', $this->plugin->getMethod());

        $this->filesystem->shouldReceive('writeStream')->with('newpath', m::typeOf('resource'))->andReturn(true);

        $this->fileUpload->shouldReceive('isOk')->andReturn(true);
        $this->fileUpload->shouldReceive('getTemporaryFile')->andReturn($this->tmpFilePath);

        $file = $this->plugin->handle('newpath', $this->fileUpload);
        $this->assertInstanceOf(\League\Flysystem\File::class, $file);
    }

    public function testPluginBadFile()
    {
        $this->fileUpload->shouldReceive('isOk')->andReturn(false);

        $this->assertFalse($this->plugin->handle('newpath', $this->fileUpload));
    }

    public function testPluginWriteFail()
    {
        $this->filesystem->shouldReceive('writeStream')->with('newpath', m::typeOf('resource'))->andReturn(false);

        $this->fileUpload->shouldReceive('isOk')->andReturn(true);
        $this->fileUpload->shouldReceive('getTemporaryFile')->andReturn($this->tmpFilePath);

        $this->assertFalse($this->plugin->handle('newpath', $this->fileUpload));
    }

    public function testPluginWriteFileExistsException()
    {
        $this->expectException(\League\Flysystem\FileNotFoundException::class);
        $this->filesystem->shouldReceive('writeStream')->with('newpath', m::typeOf('resource'))->andThrow(\League\Flysystem\FileNotFoundException::class);
        
        $this->fileUpload->shouldReceive('isOk')->andReturn(true);
        $this->fileUpload->shouldReceive('getTemporaryFile')->andReturn($this->tmpFilePath);
        $this->plugin->handle('newpath', $this->fileUpload);
    }

    public function testPluginWriteInvalidArgumentException()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->filesystem->shouldReceive('writeStream')->with('newpath', m::typeOf('resource'))->andThrow(\InvalidArgumentException::class);

        $this->fileUpload->shouldReceive('isOk')->andReturn(true);
        $this->fileUpload->shouldReceive('getTemporaryFile')->andReturn($this->tmpFilePath);
        $this->plugin->handle('newpath', $this->fileUpload);
    }

}
