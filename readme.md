# Flysystem

## Configuration
```
extensions:
    flysystem: Slts\Flysystem\DI\FlysystemExtension
    
flysystem:
    adapters:
        default: League\Flysystem\Adapter\NullAdapter
        localwww:
            class: League\Flysystem\Adapter\Local(%wwwDir%/images)
            autowired: true
        ftp:
            class League\Flysystem\Adapter\Ftp(%filesystem.ftp%)
        
    filesystems:
        default: 
            adapter: localwww
            plugins: 
                - League\Flysystem\Plugin\ListFiles
                - League\Flysystem\Plugin\ForcedCopy
            config: 
                disable_asserts: true        
        local:
            adapter: default
            autowired: true # only one adapter, filesystem and mount manager can be autowired
        ftp: 
            adapter: ftp

    mountManagers: 
        local2ftp:
            filesystems:
                renamed: local # will use 'renamed' as key
                - ftp # will use its name 'ftp' as key
            autowired: true
```

## Missing features
- Cache